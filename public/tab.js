var TrimpsAB = {};
TrimpsAB.trimpsVersion = 4.72;
TrimpsAB.active = true;
TrimpsAB.timeout = 1000;

TrimpsAB.mainLoop = function () {
    //console.log("run");
    
    if (((game.resources.food.owned / getMaxForResource("food")) * 100) > 98) {
        buyBuilding("Barn");
    }
    
    if (((game.resources.wood.owned / getMaxForResource("wood")) * 100) > 98) {
        buyBuilding("Shed");
    }
    
    if (((game.resources.metal.owned / getMaxForResource("metal")) * 100) > 98) {
        buyBuilding("Forge");
    }
    
    TrimpsAB.startLoop();
};

TrimpsAB.startLoop = function () {
    if (TrimpsAB.active === true) {
        setTimeout(TrimpsAB.mainLoop, TrimpsAB.timeout);
    } else {
        return;
    }
};

TrimpsAB.startAB = function () {
    console.log("Trimps AB loaded for Trimps v:" + TrimpsAB.trimpsVersion);
    console.log("food: " + (game.resources.food.owned / getMaxForResource("food")) * 100);
    console.log("wood: " + (game.resources.wood.owned / getMaxForResource("wood")) * 100);
    console.log("metal: " + (game.resources.metal.owned / getMaxForResource("metal")) * 100);
    setTimeout(TrimpsAB.startLoop, 3000);
};

TrimpsAB.startAB();