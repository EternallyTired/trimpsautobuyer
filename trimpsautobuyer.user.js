// ==UserScript==
// @name         Trimps Auto Buyer
// @namespace    https://gitlab.com/eternallytired/trimpsautobuyer
// @version      1
// @description  Simple script to auto buy storage in Trimps
// @author       EternallyTired
// @match        https://trimps.github.io/
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var script = document.createElement('script');
    script.setAttribute('type','text/javascript');
    script.setAttribute('src','https://eternallytired.gitlab.io/trimpsautobuyer/tab.js');
    document.head.appendChild(script);
})();